# Andamio Platform
## Front End Template 2023

This repository is a pre-release version of the Andamio Front End Template. Our team is currently building a more robust set of tooling for creating an Andamio instance. For now, you can learn about Andamio by creating a fork of this repo. We'll continue to post updates in the upcoming weeks.

### Quick Start
```bash
git clone https://gitlab.com/gimbalabs/andamio/andamio-lms-front-end-template
cd andamio-lms-front-end-template
yarn
yarn dev
```

### Full Docs
https://gimbalabs-docs.vercel.app/docs/category/ppbl-front-end-template

### About Branches
https://gimbalabs-docs.vercel.app/docs/project-based-learning/plutus-pbl/front-end-template/project-branches