import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "../module.json";
import Docs from "./Docs.mdx";


export default function Lesson() {
  const slug = "2014";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={201} sltId="201.4" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs />
    </LessonLayout>
  );
}