import { Box, Text, Heading, Grid, GridItem, Divider, Link as CLink, UnorderedList, ListItem, Center } from "@chakra-ui/react";
import * as React from "react";
import VideoComponent from "./VideoComponent";

type Props = {
  lessonData: any;
};
const LessonIntroAndVideo: React.FC<Props> = ({ lessonData }) => {
  return (
    <>
      <Grid mx="auto" fontSize="lg" fontWeight="medium" templateColumns="repeat(2, 1fr)" gap={6}>
        <GridItem w="95%" mx="auto" colSpan={[2, 1]}>
          {lessonData.introduction.map((sentence: string, index: number) => (
            <Text key={index} py="3">
              {sentence}
            </Text>
          ))}
          {lessonData.links.length > 0 && (
            <Box mt="2" p="2" bg="theme.lightGray" color="white">
              <Heading pt="2" pb="1" size="md">
                LINKS
              </Heading>
              <UnorderedList>
                {lessonData.links.map((link: { linkText: string; url: string }, index: number) => (
                  <ListItem key={index} fontSize="md" py="1">
                    <CLink href={link.url} target="_blank">
                      {link.linkText}
                    </CLink>
                  </ListItem>
                ))}
              </UnorderedList>
            </Box>
          )}
        </GridItem>
        { lessonData.youtubeId.length == 11 ? (
          <GridItem colSpan={[2, 1]}>
            <VideoComponent videoId={lessonData.youtubeId}>{lessonData.videoHeading}</VideoComponent>
          </GridItem>
        ) : (
          <GridItem colSpan={[2, 1]}>
            <Center flexDirection="column" h="100%" bg="theme.blue">
              <Text color="theme.dark">Video coming soon</Text>
              <Text><CLink href="/live-coding" color="theme.lightGray" fontSize="xl">{lessonData.videoHeading}</CLink></Text>
            </Center>
          </GridItem>
        )}
      </Grid>
      <Divider py="5" />
    </>
  );
};

export default LessonIntroAndVideo;
